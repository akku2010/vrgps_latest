import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeviceSettingsPage } from './device-settings';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DeviceSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(DeviceSettingsPage),
    TranslateModule.forChild()
  ],
})
export class DeviceSettingsPageModule {}
