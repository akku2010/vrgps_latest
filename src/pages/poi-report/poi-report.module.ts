import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { POIReportPage } from './poi-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    POIReportPage
  ],
  imports: [
    IonicPageModule.forChild(POIReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class POIReportPageModule {}
