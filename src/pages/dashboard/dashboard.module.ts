import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { TranslateModule } from '@ngx-translate/core';
import { ProgressBarModule } from '../add-devices/progress-bar/progress-bar.module';

@NgModule({
  declarations: [
    DashboardPage
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    TranslateModule.forChild(),
    ProgressBarModule
  ],
  exports: []
})
export class DashboardPageModule {}
