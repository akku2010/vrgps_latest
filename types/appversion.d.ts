export function composePattern(pattern: any, callback: any): void;
export function composePatternSync(pattern: any): any;
export function getAppVersion(callback: any): void;
export function getAppVersionSync(): any;
